#!/usr/bin/python
import logging
from TwitterAPI import TwitterAPI
import json
import csv
import os.path
import fnmatch


def locate(pattern, pathpattern="*", root=os.curdir):
    '''Locate all files matching supplied filename pattern in and below
    supplied root directory.'''
    for path, dirs, files in os.walk(os.path.abspath(root)):
        if fnmatch.fnmatch(path, pathpattern):
            for filename in fnmatch.filter(files, pattern):
                return os.path.join(path, filename)

def load_json(json_file):
	if os.path.exists(json_file) and os.access(json_file, os.R_OK):
		return json.loads(open(json_file).read())
	else: 
		return None

def app_connect(app_config):
	try:
		return TwitterAPI(app_config_json['credential']['CONSUMER_KEY'],
                	app_config_json['credential']['CONSUMER_SECRET'],
                 	app_config_json['credential']['ACCESS_TOKEN_KEY'],
                 	app_config_json['credential']['ACCESS_TOKEN_SECRET'])
	except Exception as e:
		logging.debug("TwitterAPI Exception: %s" + str(e))
		exit(1)

def validate_track_term(track):
	return True if track != '' else False

def log_twit(twit,writer):
	csv = {
		'created_at': "created_at:%s" % twit['created_at'],
		'screen_name': "screen_name:%s" % twit['user']['screen_name'],
		'coordinates': "coordinates:%s" % twit['coordinates'],
		'retweet_count': "retweet_count:%s" % twit['retweet_count'],
		'text': "text:%s" % twit['text'].encode("utf-8").replace("\n"," ")
	}
	writer.writerow(csv)

if __name__ == "__main__":
    	logging.basicConfig(filename='/tmp/ews.txt', level=logging.DEBUG)
    	logging.info('Started')

	HOMEDIR = './'
	app_config_json = None
	country_config_json = None

	# load
	config = None
	config = locate(pattern='app_config.json',root=HOMEDIR)
	if config:
		app_config_json = load_json(config)
		logging.info('Loaded: app_config_json')
	else:
		logging.debug('Loading Failed: app_config_json')
		exit(1)

	config = None
	config = locate(pattern='config.json',root=HOMEDIR)

        if config:
		country_config_json = load_json(config)
		logging.info('Loaded: config.json')
	else:
		logging.debug('Loading Failed: config_json')
		exit(1)

	if app_config_json and country_config_json:
        	APP_CONFIG = "%s%s" % (app_config_json['path'],app_config_json['log']['APP_CONFIG'])
       	 	COUNTRY_CONFIG = "%s%s" % (app_config_json['path'],app_config_json['log']['COUNTRY_CONFIG'])
       	 	TWIT_FD = "%s%s" % (app_config_json['path'],app_config_json['log']['TWIT_FD'],)

		if validate_track_term(country_config_json['et']['track']):
		
			TRACK_TERM = ','.join([str(search) for search in country_config_json['et']['track']])
			logging.info('TWITTER: Connecting')
			twitter_conn = app_connect(app_config_json)
			logging.info('TWITTER: Connected')
			if twitter_conn:
				logging.info('TWITTER_REQUEST: Starting')
				req = twitter_conn.request('statuses/filter', {'track': TRACK_TERM})
				logging.info('TWITTER_REQUEST: Requsted')
                                try:
                               		with open(TWIT_FD, 'a') as log_twit_fd:
						writer = csv.DictWriter(
                    					log_twit_fd, fieldnames=app_config_json['log']['FIELDS'], delimiter=',', quoting=csv.QUOTE_ALL)
						logging.info('LOG_TWIT: Started')
						for twit in req:
							if 'text' in twit:
								log_twit(twit,writer)
				except Exception as e:
					logging.debug("LOG_TWIT: Error opening file: %s" + str(e))
					exit(1)
